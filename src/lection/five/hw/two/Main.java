package lection.five.hw.two;

import lection.five.hw.two.enums.Gender;
import lection.five.hw.two.exceptions.GroupOverflowException;
import lection.five.hw.two.group.Group;
import lection.five.hw.two.group.GroupFileStorage;
import lection.five.hw.two.student.Student;
import lection.five.hw.two.student.StudentBuilder;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        String groupName = "Journalism";
        Group group = new Group(groupName);

        Student student01 = new Student("Volodymyr", "Petrenko", Gender.MALE, 1, groupName);
        Student student02 = new Student("Vsevolod", "Andriienko", Gender.MALE, 2, groupName);
        Student student03 = new Student("Rostyslav", "Petruk", Gender.MALE, 3, groupName);
        Student student04 = new Student("Liudmyla ", "Andriiuk", Gender.FEMALE, 4, groupName);
        Student student05 = new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName);
        Student student06 = new Student("Vira", "Andriievych", Gender.FEMALE, 6, groupName);
        Student student07 = new Student("Nadiia", "Petriv", Gender.FEMALE, 7, groupName);
        Student student08 = new Student("Liubov", "Andriiv", Gender.FEMALE, 8, groupName);
        Student student09 = new Student("Ihor", "Petrash", Gender.MALE, 9, groupName);
        Student student10 = new Student("Oleh", "Andriiash", Gender.MALE, 10, groupName);

        addStudentToGroup(group, student01);
        addStudentToGroup(group, student02);
        addStudentToGroup(group, student03);
        addStudentToGroup(group, student04);
        addStudentToGroup(group, student05);
        addStudentToGroup(group, student06);
        addStudentToGroup(group, student07);
        addStudentToGroup(group, student08);
        addStudentToGroup(group, student09);
        addStudentToGroup(group, student10);

        printGroupInfo(group);

        GroupFileStorage gfs = new GroupFileStorage();

        try {
            gfs.saveGroupToCSV(group);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = new File("./Journalism.csv");
        Group group2;
        try {
            group2 = gfs.loadGroupFromCSV(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        printGroupInfo(group2);

        File workFolder = new File(".");
        System.out.println(gfs.findFileByGroupName("Journalism", workFolder));
    }

    private static void printGroupInfo(Group group) {
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
    }

    private static void addStudentToGroup(Group group, Student student) {
        try {
            StudentBuilder.addToGroup(student, group);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }
    }
}